-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 05:14 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lion`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(5) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `id_level` int(5) NOT NULL,
  `link_ktp` varchar(50) NOT NULL,
  `saldo` int(7) NOT NULL,
  `status` enum('tidak aktif','aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_admin`, `id_level`, `link_ktp`, `saldo`, `status`) VALUES
(1, 'midmines', 'souiz', 'Muhammad Ridwan Edi Pamungkas', 1, '', 1374250, 'aktif'),
(2, 'aleph', 'nubdoto', 'Aleph Tsumiki', 2, '', 0, 'tidak aktif'),
(3, 'andre', 'andrepass', 'Agus Andreyansyah', 3, '', 0, 'tidak aktif'),
(4, 'super', 'super', 'Saya Super Admin', 1, '', 5000000, 'aktif'),
(5, 'admin', 'admin', 'Saya Admin Gan', 2, '', 353000, 'aktif'),
(6, 'petugas', 'petugas', 'Saya Petugas Gan', 3, '', 0, 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `admin_saldo`
--

CREATE TABLE `admin_saldo` (
  `id` int(5) NOT NULL,
  `id_admin` int(5) NOT NULL,
  `saldo` int(7) NOT NULL,
  `tanggal` int(2) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_saldo`
--

INSERT INTO `admin_saldo` (`id`, `id_admin`, `saldo`, `tanggal`, `bulan`, `tahun`) VALUES
(5, 2, 22, 6, 4, 2019),
(6, 1, 1004, 6, 4, 2019),
(7, 1, 1000, 6, 4, 2019),
(8, 1, 1000, 6, 4, 2019),
(9, 1, 1000, 6, 4, 2019),
(10, 1, 2000, 6, 4, 2019),
(11, 2, 1000, 6, 4, 2019),
(12, 1, 1, 6, 4, 2019);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(10) NOT NULL,
  `id_tagihan` int(10) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `bayar` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `id_tagihan`, `bulan`, `tahun`, `bayar`) VALUES
(1, 2, 4, 2019, 222000),
(2, 4, 4, 2019, 62000),
(3, 7, 4, 2019, 250750),
(4, 8, 4, 2019, 3000),
(5, 11, 4, 2019, 372000),
(6, 10, 4, 2019, 147000),
(7, 10, 4, 2019, 147000);

-- --------------------------------------------------------

--
-- Table structure for table `bank_account`
--

CREATE TABLE `bank_account` (
  `id` int(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` enum('tidak aktif','aktif') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_account`
--

INSERT INTO `bank_account` (`id`, `username`, `password`, `status`) VALUES
(1, 'thanksforujikom', '2019', 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `bank_saldo_request`
--

CREATE TABLE `bank_saldo_request` (
  `id` int(5) NOT NULL,
  `id_admin` int(5) NOT NULL,
  `saldo` int(7) NOT NULL,
  `status` enum('request','decline','success') NOT NULL,
  `tanggal` int(2) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank_saldo_request`
--

INSERT INTO `bank_saldo_request` (`id`, `id_admin`, `saldo`, `status`, `tanggal`, `bulan`, `tahun`) VALUES
(1, 1, 1, 'decline', 6, 4, 2019),
(2, 1, 1000000, 'success', 6, 4, 2019);

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(5) NOT NULL,
  `nama_level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'superadmin'),
(2, 'admin'),
(3, 'petugas');

-- --------------------------------------------------------

--
-- Table structure for table `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id_pelanggan` int(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nomor_kwh` bigint(20) NOT NULL,
  `nama_pelanggan` varchar(30) NOT NULL,
  `no_hp` varchar(16) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `id_tarif` int(5) NOT NULL,
  `status` enum('tidak aktif','aktif') NOT NULL,
  `id_admin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pelanggan`
--

INSERT INTO `pelanggan` (`id_pelanggan`, `username`, `password`, `nomor_kwh`, `nama_pelanggan`, `no_hp`, `alamat`, `id_tarif`, `status`, `id_admin`) VALUES
(1, 'midmines', 'nerushimyj', 538311634223, 'Muhammad Ridwan Edi Pamungkas', '087873814860', 'Jln. Cendana Blok D5/No7', 2, 'aktif', 1),
(2, 'adul', 'adul2069', 14044, 'Adul Gustono Pramudityo', '087183765910', 'Jln. Cemara Indah Permata Sari Blok A8/No39', 3, 'tidak aktif', 1),
(3, 'ridwan', 'ridwanaja', 132123111, 'Ridwan kun', '081938293331', 'Jln. Cendana Aja', 4, 'tidak aktif', 1),
(7, 'harsa', 'sayaharsa', 140451, 'Harsa Aditya', '081938291133', 'ditempat', 3, 'tidak aktif', 0),
(8, 'psatu', 'pelanggan', 1, 'Pelanggan satu', '0811111111111', 'Disuatu tempat yang berisi satu orang saja', 1, 'aktif', 1),
(9, 'pdua', 'pelanggan', 2, 'Pelanggan dua', '082222222222', 'Disuatu tempat yang jauh ada dua orang', 2, 'tidak aktif', 1),
(10, 'budi', 'budi', 1829587186, 'Budi', '081928392745', 'Perum. Pagelaran Blok D5/No7', 4, 'aktif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` bigint(33) NOT NULL,
  `id_tagihan` int(10) NOT NULL,
  `id_pelanggan` int(15) NOT NULL,
  `tanggal_pembayaran` int(2) NOT NULL,
  `bulan_bayar` int(2) NOT NULL,
  `biaya_admin` int(10) NOT NULL,
  `total_bayar` int(10) NOT NULL,
  `id_admin` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_tagihan`, `id_pelanggan`, `tanggal_pembayaran`, `bulan_bayar`, `biaya_admin`, `total_bayar`, `id_admin`) VALUES
(3, 3, 1, 22, 5, 2000, 25000, 1),
(4, 5, 3, 6, 5, 3000, 9000, 2),
(15, 1, 1, 5, 4, 2000, 0, 1),
(16, 5, 3, 5, 4, 2000, 7000, 1),
(17, 2, 1, 5, 4, 2000, 2000, 1),
(18, 4, 1, 6, 4, 2000, 0, 2),
(20, 8, 7, 6, 4, 2000, 3000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penggunaan`
--

CREATE TABLE `penggunaan` (
  `id_penggunaan` int(15) NOT NULL,
  `id_pelanggan` int(15) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `meter_awal` int(10) NOT NULL,
  `meter_akhir` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penggunaan`
--

INSERT INTO `penggunaan` (`id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `meter_awal`, `meter_akhir`) VALUES
(1, 1, 3, 2019, 100310, 101800),
(2, 1, 4, 2019, 101800, 102900),
(3, 3, 5, 2019, 102900, 103600),
(4, 1, 5, 2019, 102900, 103900),
(5, 1, 6, 2019, 103900, 104200),
(6, 2, 4, 2019, 1, 2),
(11, 7, 4, 2019, 1, 5),
(12, 10, 2, 2019, 5500, 6000),
(13, 10, 3, 2019, 6000, 7400),
(18, 10, 4, 2019, 7400, 8000);

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id_tagihan` int(10) NOT NULL,
  `id_penggunaan` int(15) NOT NULL,
  `id_pelanggan` int(15) NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `jumlah_meter` varchar(30) NOT NULL,
  `status` enum('Belum Dibayar','Diproses','Lunas') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagihan`
--

INSERT INTO `tagihan` (`id_tagihan`, `id_penggunaan`, `id_pelanggan`, `bulan`, `tahun`, `jumlah_meter`, `status`) VALUES
(1, 1, 1, 3, 2019, '100310-101800', 'Lunas'),
(2, 2, 1, 4, 2019, '101800-102900', 'Lunas'),
(3, 4, 1, 5, 2019, '102900-103900', 'Lunas'),
(4, 5, 1, 6, 2019, '103900-104200', 'Lunas'),
(5, 3, 3, 5, 2019, '102900-103600', 'Lunas'),
(6, 6, 2, 5, 2019, '3', 'Diproses'),
(8, 11, 7, 4, 2019, '1-5', 'Lunas'),
(10, 12, 10, 2, 2019, '5500-6000', 'Lunas'),
(11, 13, 10, 3, 2019, '6000-7400', 'Belum Dibayar'),
(12, 18, 10, 4, 2019, '7400-8000', 'Belum Dibayar');

-- --------------------------------------------------------

--
-- Table structure for table `tarif`
--

CREATE TABLE `tarif` (
  `id_tarif` int(5) NOT NULL,
  `daya` int(10) NOT NULL,
  `tarifperkwh` int(10) NOT NULL,
  `biaya_keterlambatan` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tarif`
--

INSERT INTO `tarif` (`id_tarif`, `daya`, `tarifperkwh`, `biaya_keterlambatan`) VALUES
(1, 450, 123, 5000),
(2, 900, 200, 10000),
(3, 1300, 250, 15000),
(4, 2200, 250, 20000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `admin_saldo`
--
ALTER TABLE `admin_saldo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_account`
--
ALTER TABLE `bank_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_saldo_request`
--
ALTER TABLE `bank_saldo_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id_pelanggan`),
  ADD KEY `id_tarif` (`id_tarif`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`),
  ADD KEY `id_tagihan` (`id_tagihan`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `penggunaan`
--
ALTER TABLE `penggunaan`
  ADD PRIMARY KEY (`id_penggunaan`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_pelanggan_2` (`id_pelanggan`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id_tagihan`),
  ADD KEY `id_pelanggan` (`id_pelanggan`),
  ADD KEY `id_penggunaan` (`id_penggunaan`);

--
-- Indexes for table `tarif`
--
ALTER TABLE `tarif`
  ADD PRIMARY KEY (`id_tarif`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admin_saldo`
--
ALTER TABLE `admin_saldo`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `bank_account`
--
ALTER TABLE `bank_account`
  MODIFY `id` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bank_saldo_request`
--
ALTER TABLE `bank_saldo_request`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id_pelanggan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` bigint(33) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `penggunaan`
--
ALTER TABLE `penggunaan`
  MODIFY `id_penggunaan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id_tagihan` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tarif`
--
ALTER TABLE `tarif`
  MODIFY `id_tarif` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Constraints for table `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD CONSTRAINT `pelanggan_ibfk_1` FOREIGN KEY (`id_tarif`) REFERENCES `tarif` (`id_tarif`);

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_ibfk_3` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`),
  ADD CONSTRAINT `pembayaran_ibfk_5` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id_admin`),
  ADD CONSTRAINT `pembayaran_ibfk_6` FOREIGN KEY (`id_tagihan`) REFERENCES `tagihan` (`id_tagihan`);

--
-- Constraints for table `penggunaan`
--
ALTER TABLE `penggunaan`
  ADD CONSTRAINT `penggunaan_ibfk_1` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`);

--
-- Constraints for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD CONSTRAINT `tagihan_ibfk_2` FOREIGN KEY (`id_pelanggan`) REFERENCES `pelanggan` (`id_pelanggan`),
  ADD CONSTRAINT `tagihan_ibfk_3` FOREIGN KEY (`id_penggunaan`) REFERENCES `penggunaan` (`id_penggunaan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
