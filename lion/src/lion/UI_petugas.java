/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Midmines
 */
public class UI_petugas extends javax.swing.JFrame {
    Connection con;
    ResultSet res;
    Statement sta;
    connect co = new connect();
    
    Date date;
    String format_day,format_month,format_year;
    static String id_pelanggan,id_penggunaan;
    SimpleDateFormat day,month,year;
    /**
     * Creates new form UI_petugas
     */
    public UI_petugas() {
        co.ConnectDB();
        con = co.c_co;
        sta = co.c_st;
        
        date = new Date();
        format_day = "d";
        format_month = "M";
        format_year = "yyyy";
        day = new SimpleDateFormat(format_day);
        month = new SimpleDateFormat(format_month);
        year = new SimpleDateFormat(format_year);
        
        initComponents();
        bulan.setText(month.format(date));
        tahun.setText(year.format(date));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        kwh = new javax.swing.JTextField();
        bulan = new javax.swing.JTextField();
        tahun = new javax.swing.JTextField();
        meter_awal = new javax.swing.JTextField();
        meter_akhir = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("PETUGAS");
        setResizable(false);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        jLabel9.setText("Midmines");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 9)); // NOI18N
        jLabel3.setText("Powered by");

        jLabel1.setText("Nomor KWH");

        jLabel2.setText("Bulan");

        jLabel4.setText("Tahun");

        jLabel5.setText("Meter Awal");

        jLabel6.setText("Meter Akhir");

        kwh.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                kwhKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                kwhKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                kwhKeyTyped(evt);
            }
        });

        bulan.setEditable(false);

        tahun.setEditable(false);

        meter_awal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                meter_awalKeyPressed(evt);
            }
        });

        meter_akhir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                meter_akhirKeyPressed(evt);
            }
        });

        jButton1.setText("Submit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Logout");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(kwh)
                            .addComponent(bulan)
                            .addComponent(tahun)
                            .addComponent(meter_awal)
                            .addComponent(meter_akhir))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(kwh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(bulan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tahun, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(meter_awal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(meter_akhir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel3))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked

    }//GEN-LAST:event_jLabel9MouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(Integer.parseInt(meter_akhir.getText()) > Integer.parseInt(meter_awal.getText()))
        {
            try{
                try{
                    String get_admin = "SELECT DISTINCT id_pelanggan, nomor_kwh FROM pelanggan WHERE nomor_kwh='"+kwh.getText()+"'";
                    res = sta.executeQuery(get_admin);
                    if(res.next()){
                        id_pelanggan = res.getString("id_pelanggan");
                    }
                }
                catch(Exception e){
                    e.getStackTrace();
                }

                String petugas_insert = "INSERT INTO penggunaan (id_pelanggan, bulan, tahun, meter_awal, meter_akhir) VALUES ('"+id_pelanggan+"','"+bulan.getText()+"','"+tahun.getText()+"','"+meter_awal.getText()+"','"+meter_akhir.getText()+"')";
                PreparedStatement prepare_insert_penggunaan = co.c_co.prepareStatement(petugas_insert);
                prepare_insert_penggunaan.executeUpdate();

                try{
                    String get_admin = "SELECT id_penggunaan ,id_pelanggan, bulan, tahun FROM penggunaan WHERE id_pelanggan='"+id_pelanggan+"' AND bulan='"+bulan.getText()+"' AND tahun='"+tahun.getText()+"'";
                    res = sta.executeQuery(get_admin);
                    if(res.next()){
                        id_penggunaan = res.getString("id_penggunaan");
                    }
                }
                catch(Exception e){
                    e.getStackTrace();
                }

                String tagihan_insert = "INSERT INTO tagihan (id_penggunaan, id_pelanggan, bulan, tahun, jumlah_meter) VALUES ('"+id_penggunaan+"','"+id_pelanggan+"','"+bulan.getText()+"','"+tahun.getText()+"','"+meter_awal.getText()+"-"+meter_akhir.getText()+"')";
                PreparedStatement prepare_tagihan_insert = co.c_co.prepareStatement(tagihan_insert);
                prepare_tagihan_insert.executeUpdate();
                kwh.setText("");
                bulan.setText("");
                tahun.setText("");
                meter_awal.setText("");
                meter_akhir.setText("");
                JOptionPane.showMessageDialog(null, "Data berhasil di masukkan!");
            }
            catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Meter Akhir lebih rendah daripada Meter Awal!");
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.setVisible(false);
        new login().setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void kwhKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kwhKeyReleased
        
    }//GEN-LAST:event_kwhKeyReleased

    private void kwhKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kwhKeyPressed
        if(evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57 || evt.getKeyCode() == 8){
        }
        else{
            JOptionPane.showMessageDialog(null, "Maaf, tidak diperkenankan menekan tombol selain Nomor!");
            kwh.setText("");
        }
    }//GEN-LAST:event_kwhKeyPressed

    private void kwhKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_kwhKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_kwhKeyTyped

    private void meter_awalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_meter_awalKeyPressed
        if(evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57 || evt.getKeyCode() == 8){
        }
        else{
            JOptionPane.showMessageDialog(null, "Maaf, tidak diperkenankan menekan tombol selain Nomor!");
            meter_awal.setText("");
        }
    }//GEN-LAST:event_meter_awalKeyPressed

    private void meter_akhirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_meter_akhirKeyPressed
        if(evt.getKeyCode() >= 48 && evt.getKeyCode() <= 57 || evt.getKeyCode() == 8){
        }
        else{
            JOptionPane.showMessageDialog(null, "Maaf, tidak diperkenankan menekan tombol selain Nomor!");
            meter_akhir.setText("");
        }
    }//GEN-LAST:event_meter_akhirKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UI_petugas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UI_petugas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UI_petugas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UI_petugas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UI_petugas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField bulan;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField kwh;
    private javax.swing.JTextField meter_akhir;
    private javax.swing.JTextField meter_awal;
    private javax.swing.JTextField tahun;
    // End of variables declaration//GEN-END:variables
}
